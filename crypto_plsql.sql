create table msisdn as
select 
replace(SYS_CONNECT_BY_PATH(mod(level,10), '/'), '/', '') msisdn
from dual connect by level < 13;
/

-- encrypt from source table, insert into dest table
DECLARE
 enc_data RAW(2000);
 TYPE ptt IS TABLE OF RAW(2000);
 plain_data_tab ptt;
 num_key_bytes      NUMBER := 256/8;        -- key length 256 bits (32 bytes)
 key_bytes_raw      RAW (32);               -- stores 256-bit encryption key
 encryption_type    PLS_INTEGER :=          -- total encryption type
                        DBMS_CRYPTO.ENCRYPT_AES256
                      + DBMS_CRYPTO.CHAIN_CBC
                      + DBMS_CRYPTO.PAD_PKCS5;
 CURSOR c1 IS
  SELECT msisdn FROM msisdn;
BEGIN

EXECUTE IMMEDIATE 'truncate table msisdn_enc';

  OPEN c1;
  FETCH c1
   BULK COLLECT INTO plain_data_tab;
  CLOSE c1;

--   key_bytes_raw := DBMS_CRYPTO.RANDOMBYTES (num_key_bytes);
   key_bytes_raw := UTL_I18N.STRING_TO_RAW (rpad('oracle', 32, '0'), 'AL32UTF8');

  FORALL i IN plain_data_tab.first .. plain_data_tab.last
   INSERT INTO msisdn_enc VALUEs(DBMS_CRYPTO.ENCRYPT
      (
         src => plain_data_tab(i),
         typ => encryption_type,
         key => key_bytes_raw
      ));
   COMMIT;
END;
/

-- decrypt from encrypted table, insert plain text
DECLARE
 enc_data RAW(2000);
 TYPE ett IS TABLE OF RAW(2000);
 encr_data_tab ett;
 num_key_bytes      NUMBER := 256/8;        -- key length 256 bits (32 bytes)
 key_bytes_raw      RAW (32);               -- stores 256-bit encryption key
 encryption_type    PLS_INTEGER :=          -- total encryption type
                        DBMS_CRYPTO.ENCRYPT_AES256
                      + DBMS_CRYPTO.CHAIN_CBC
                      + DBMS_CRYPTO.PAD_PKCS5;
 CURSOR c1 IS
  SELECT encrypted_data FROM msisdn_enc /*ORDER BY 1 DESC FETCH FIRST 1 ROWS ONLY*/;
BEGIN

EXECUTE IMMEDIATE 'truncate table msisdn_dec';

  OPEN c1;
  FETCH c1
   BULK COLLECT INTO encr_data_tab;
  CLOSE c1;

--   key_bytes_raw := DBMS_CRYPTO.RANDOMBYTES (num_key_bytes);
   key_bytes_raw := UTL_I18N.STRING_TO_RAW (rpad('oracle', 32, '0'), 'AL32UTF8');
/*dbms_output.put_line(

  DBMS_CRYPTO.DECRYPT
      (
         src => encr_data_tab(encr_data_tab.first),
         typ => encryption_type,
         key => key_bytes_raw
      )
      );*/

  FORALL i IN encr_data_tab.first .. encr_data_tab.last
   INSERT INTO msisdn_dec VALUEs(
  DBMS_CRYPTO.DECRYPT
      (
         src => encr_data_tab(i),
         typ => encryption_type,
         key => key_bytes_raw
      ));
   COMMIT;
exception
   when others then
        dbms_output.put_line( dbms_utility.format_error_backtrace );
END;
/
